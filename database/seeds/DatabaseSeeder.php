<?php

use Illuminate\Database\Seeder;
use App\Models\Item;
use App\User;
use Faker\Factory;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Factory::create();

for ($i=0; $i < rand(1,10); $i++) {
      $item = new Item();
      $item->title = $faker->word;
      $item->content = $faker->catchPhrase;
      $item->save();
    }

      $user = new User();
      $user->name = $faker->name;
      $user->email = $faker->email;
      $user->password = $faker->password;
      $user->handle = $faker->userName;
      $user->save();    }
}
