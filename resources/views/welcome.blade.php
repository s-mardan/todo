<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>To-Do Exercise!!</title>
  </head>
  <body>

</html>

<header class="bg1">

<h1>To-do List</h1>
</header>
<ul>
    <?php foreach($items as $item): ?>
        <li>
            Title: <?php echo $item->title ?>
        </li>
        <li>
            Description: <?php echo $item->content ?>
        </li>
        <li>
            Date: <?php echo $item->updated_at ?>
        </li>
    <?php endforeach; ?>
</ul>

  </body>
