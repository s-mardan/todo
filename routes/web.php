<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Faker\Factory;
use App\Models\Item;
use App\User;


Route::get('/about', function() {
    return view('about');
});

Route::get('/', function () {
    $faker = Factory::create();

    $users = User::all();
    // $user1 = new User();
    // $user1->name = $faker->name;

    $items = Item::all();
    // $tweet1 = new Tweet();
    // $tweet1->content = $faker->text(280);



    $data = [
        'users' => $users,
        'items' => $items
    ];

    return view('welcome', $data);
});
